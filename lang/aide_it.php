<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/aide-aide?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// R
	'raccourcis' => 'Scorciatoie tipografiche',
	'raccourcis_citation' => 'Citazioni',
	'raccourcis_code' => 'Codice informatico',
	'raccourcis_glossaire' => 'Glossario esterno',
	'raccourcis_lien' => 'Collegamenti ipertestuali',
	'raccourcis_note' => 'Note a piè di pagina',
	'raccourcis_resume' => 'In sintesi',
	'raccourcis_simple' => 'Formattazione semplice'
);
