<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-aide?lang_cible=pt
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aide_description' => 'Este plugin permite incluir no SPIP uma ajuda contextual identificada por um ícone. Esta ajuda também pode ser estendida aos plugins.',
	'aide_nom' => 'Ajuda SPIP',
	'aide_slogan' => 'Ajuda online do SPIP'
);
