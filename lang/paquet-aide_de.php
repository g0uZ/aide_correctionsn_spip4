<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-aide?lang_cible=de
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// A
	'aide_description' => 'Dieses Plugin ermöglicht es, in SPIP eine kontextsensitive Hilfe einzufügen, die durch ein Icon gekennzeichnet ist. Diese Hilfe kann auch auf Plugins ausgeweitet werden. ',
	'aide_nom' => 'SPIP Hilfe',
	'aide_slogan' => 'SPIP Online-Hilfe'
);
